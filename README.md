### **Description**
Android game sokoban created as a school project

---
### **Technology**
Android

---
### **Year**
2013

---
### **Screenshots**
![03](README/03.png)
![01](README/01.png)
![02](README/02.png)
![kod4](README/kod4.jpg)
![004](README/004.png)
![014](README/014.png)
![015](README/015.png)
![016](README/016.png)

