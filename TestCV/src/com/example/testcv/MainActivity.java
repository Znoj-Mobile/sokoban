package com.example.testcv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
		 
	Button button;
	Button skore;
	Button vymazatSkore;
	Button vyberKolo;
	Button exit;
	Button about;
	
	Intent intentGame;
	Intent intentSkore;
	Intent intentVymazSkore;
	Intent intentVyberKolo;
	Intent intentAbout;
	
	public static Activity gameActivity;
	public static Activity skoreActivity;
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		addListenerOnButton();

	}
 
	public void addListenerOnButton() {
 
		final Context context = this;
 
		button = (Button) findViewById(R.id.GameButton);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				intentGame = new Intent(context, FullscreenActivity.class);
                intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intentGame);           
			}
		});
 
	
		skore = (Button) findViewById(R.id.ScoreButton);
		skore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			    intentSkore = new Intent(context, Skore.class);
			    intentSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intentSkore);   
			}
		});
		
		vymazatSkore = (Button) findViewById(R.id.EraseButton);
		vymazatSkore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				intentVymazSkore = new Intent(context, VymazatSkore.class);
				intentVymazSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intentVymazSkore);   
			}
		});
		
		vyberKolo = (Button) findViewById(R.id.ChoseButton);
		vyberKolo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				intentVyberKolo = new Intent(context, VybratKolo.class);
				intentVyberKolo.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intentVyberKolo);   
			}
		});
		
		about = (Button) findViewById(R.id.AboutButton);
	    about.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View arg0) {
				intentVyberKolo = new Intent(context, Info.class);
				intentVyberKolo.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intentVyberKolo);   
			}
	    });
	    
	    exit = (Button) findViewById(R.id.EndButton);
	    exit.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            finish();
	            System.exit(0);
	        }
	    });
	    
	    
	}
	 
}
