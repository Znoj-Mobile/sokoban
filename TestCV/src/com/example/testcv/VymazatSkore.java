package com.example.testcv;

import java.text.ParseException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class VymazatSkore extends Activity {
	
	private TableDataSource db;
	Button button;
	Button button2;
	
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vymazat_layout);
		addListenerOnButton();
    }
	
	private void addListenerOnButton() {
		// TODO Auto-generated method stub
		final Context context = this;
		
		button = (Button) findViewById(R.id.buttonAno);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				eraseAll();
				Toast.makeText(getApplicationContext(), "V�chny v�sledky byly vymaz�ny", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(context, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent); 
                            
			}
		});
		
		button2 = (Button) findViewById(R.id.buttonNe);
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent); 
                            
			}
		});
		
	}

	private void eraseAll() {
		this.db = new TableDataSource(this, true);
		try {
			this.db.eraseAll();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.layout.skore_menu, menu);
	    return true;
	}
		
	//odchytavani menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu:
	        	 Intent intent = new Intent(getBaseContext(), MainActivity.class);
                 startActivity(intent);  
 	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
}
