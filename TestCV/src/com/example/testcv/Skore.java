package com.example.testcv;
import java.text.ParseException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class Skore extends ListActivity {
	private TableDataSource db;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new TableDataSource(this, false);
	    db.open();
        populateList();
    }
	
	private void populateList() {
		this.db = new TableDataSource(this, true);
		String[] records = this.db.selectScore();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, records);
            setListAdapter(adapter);
	}
	
	//menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.layout.skore_menu, menu);
	    return true;
	}
		
	//odchytavani menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu:
	        	 Intent intent = new Intent(getBaseContext(), MainActivity.class);
                 startActivity(intent);  
 	             return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
}
