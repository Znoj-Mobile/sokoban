package com.example.testcv;

import java.text.ParseException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

public class VybratKolo extends Activity {
	
	private TableDataSource db;
	Button button;
	public static NumberPicker np;
	int value;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.vybrat_layout);

		this.db = new TableDataSource(this, true);
        
        np = (NumberPicker) findViewById(R.id.numberPicker);
        
        try {
			if(db.getLast() != null){
				np.setMaxValue((int)db.getLast().getId());
			}
			else{
				np.setMaxValue(1);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        np.setMinValue(1);

		addListenerOnButton();
    }
	
	private void addListenerOnButton() {
		// TODO Auto-generated method stub
		final Context context = this;
		
		button = (Button) findViewById(R.id.level);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {


				value = np.getValue();
				Log.w("?", "val: "+value);
				db.alterTable(0, value);
				
				Log.w("?", "val: "+value);
				Toast.makeText(getApplicationContext(), "vybr�no " + value + ". kolo", Toast.LENGTH_LONG).show();
				//Intent intent = new Intent(context, FullscreenActivity.class);
				//intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                //startActivity(intent); 
                            
			}
		});
	}
	
	//menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.layout.kolo_menu, menu);
	    return true;
	}
		
	//odchytavani menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	       	case R.id.koloMenu:
	        	 Intent intent = new Intent(getBaseContext(), MainActivity.class);
	        	 intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                 startActivity(intent); 
 	             return true;
	        case R.id.koloGame:
	        	Intent intentGame = new Intent(getBaseContext(), FullscreenActivity.class);
                intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intentGame);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
}
