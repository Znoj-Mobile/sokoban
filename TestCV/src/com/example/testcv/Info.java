package com.example.testcv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class Info extends Activity{

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_layout);
        TextView tv = (TextView) findViewById(R.id.text);
        tv.setText("P�edm�t: TAMZ - Tvorba aplikac� pro mobiln� za��zen�\n" +
        		"Rok: 2013\n" +
        		"Cvi��c�: Ing. Du�an Fedor��k, Ph.D.\n" + 
        		"Autor programu: Ji�� Znoj\n");
    }
	
	//menu
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
		    MenuInflater inflater = getMenuInflater();
		    inflater.inflate(R.layout.skore_menu, menu);
		    return true;
		}
			
		//odchytavani menu
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle item selection
		    switch (item.getItemId()) {
		        case R.id.menu:
		        	 Intent intent = new Intent(getBaseContext(), MainActivity.class);
	                 startActivity(intent);  
	 	            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
}
